import React, { Component, PropTypes } from 'react';
const { object, func, string, array } = PropTypes;
import { connect } from 'react-redux';

import connectData from 'helpers/connectData';

import { login, logout } from 'redux/modules/currentUser';
import { load as loadSurveys } from 'redux/modules/surveys';
import { Link } from 'react-router';


function fetchData(getState, dispatch) {
  const token = getState().currentUser.token;
  return token ? dispatch(loadSurveys()) : Promise.resolve();
}

@connectData(fetchData)
@connect(
  ({ currentUser, surveys }) => ({ token: currentUser.token, surveys: surveys.list}),
  { login, logout, loadSurveys}
)
export default class App extends Component {
  static propTypes = {
    login: func.isRequired,
    logout: func.isRequired,
    loadSurveys: func.isRequired,
    surveys: array.isRequired,
    token: string,
    children: object.isRequired,
  };

  componentWillReceiveProps(newProps) {
    if (!this.props.token && newProps.token) {
      this.props.loadSurveys();
    }
  }


  handleLogin = () => {
    this.props.login({
      username: 'Enjeru',
      password: 'cTF3MmUzcjQ=',
    });
  };

  handleLogout = () => {
    this.props.logout();
  };

  render() {
    const { token, surveys } = this.props;
    return (
      <div>
        { token }
        { !token ? <button onClick={this.handleLogin}>Login</button> : <button onClick={this.handleLogout}>Logout</button> }
        <Link to="/user">user component</Link>
        { this.props.children }
        { surveys.length ?
          (
            <ul>
              { surveys.map(({ title, _id }) => <li key={_id}>{title}</li>) }
            </ul>
          ) : null
        }
        
      </div>
    );
  }
}
