import React, { Component, PropTypes } from 'react';
import { reduxForm } from 'redux-form';
import { connect } from 'react-redux';

import connectData from 'helpers/connectData';

import { load as loadUsers } from 'redux/modules/users';
import { save as saveUser } from 'redux/modules/users';

// const { object, func, string, array } = PropTypes;
// import { connect } from 'react-redux';
function fetchData(getState, dispatch) {
  const token = getState().currentUser.token;
  return token ? dispatch(loadUsers()) : Promise.resolve();
}

const validationRules = [
  {
    field: 'username',
    required: true,
    message: 'Required',
    match: [
      {regExp: /^.{5,10}$/, message: 'username not less then 5 more than 10'}
    ]
  },
  {
    field: 'email',
    required: true,
    message: 'Required',
    match: [
      {regExp: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i, message: 'Invalid Email Adress'}
    ]
  },
  {
    field: 'firstName',
    required: true,
    message: 'Required',
  },
  {
    field: 'lastName',
    required: true,
    message: 'Required',
  },
  {
    field: 'phone',
    required: true,
    message: 'Required',
    // @TODO phone pattern validation
  },
  {
    field: 'password',
    required: true,
    message: 'Required',
    // @TODO password pattern validation
    match: [
      {regExp: /^.{8,15}$/, message: 'Length for password min 8 max 15 charachters'},
      {regExp: /\d/, message: 'Must contain numbers'},
      {regExp: /[A-Z]/, message: 'Must contain caps letters'},
      {regExp: /[!@#$%^&*()_]/, message: 'Must contain others charachters'},
    ],
  },
  {
    field: 'confirmpass',
    required: true,
    message: 'Required',
  },

];
const validate = values => {
  const errors = {};
  validationRules.map((rule)=>{
    if (rule.required && !values[rule.field]) {
      errors[rule.field] = rule.message;
      return;
    }
    rule.match && rule.match.map((item)=>{
      if (!item.regExp.test(values[rule.field])) {
        errors[rule.field] = errors[rule.field] ? (errors[rule.field] + ' ' + item.message) : item.message;
      }
    });
  });
  if (!errors.password && (values.password !== values.confirmpass)) {
    errors.confirmpass = 'should match with password';
  }
  if (values.permission === 'fieldWorker') {
    errors.email = null;
  }
  return errors;
};

@connectData(fetchData)
@connect(
  ({ users }) => ({ users: users.list}),
  { loadUsers, saveUser }
)

class User extends Component {
  static propTypes = {
    fields: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    resetForm: PropTypes.func.isRequired,
    submitting: PropTypes.bool.isRequired,
    users: PropTypes.array.isRequired,
    saveUser: PropTypes.func.isRequired,
  }

  handleSubmit(data) {
    console.log(arguments);
    this.props.saveUser(data);
  }

  render() {
    const {fields: {firstName, lastName, email, permission, username, phone, password, confirmpass}, handleSubmit, users} = this.props;
    return (
      <form onSubmit={handleSubmit(this.handleSubmit.bind(this))}>

        <div>
          <label>First Name</label>
          <input type="text" placeholder="First Name" {...firstName}/>
          {firstName.touched && firstName.error && <div className="validation-message">{firstName.error}</div>}
        </div>

        <div>
          <label>Last Name</label>
          <input type="text" placeholder="Last Name" {...lastName}/>
          {lastName.touched && lastName.error && <div className="validation-message">{lastName.error}</div>}
        </div>

        <div>
          <label>Permission</label>
          <select type="text" placeholder="Permission" {...permission} value={permission.value || ''}>
            <option value="admin">Admin</option>
            <option value="operator">Operator</option>
            <option value="fieldWorker">Field worker</option>
          </select>
        </div>

        <div>
          <label>User Name</label>
          <input type="text" placeholder="New User Name" {...username}/>
          {username.touched && username.error && <div className="validation-message">{username.error}</div>}
        </div>

        <div>
          <label>Phone Number</label>
          <input type="number" placeholder="Phone Number" {...phone}/>
          {phone.touched && phone.error && <div className="validation-message">{phone.error}</div>}
        </div>

        {permission.value !== "fieldWorker" ?
          <div>
            <label>Email</label>
            <input type="email" placeholder="Email" {...email}/>
            {email.touched && email.error && <div className="validation-message">{email.error}</div>}
          </div>
        : ''}

        <div>
          <label>Password</label>
          <input type="password" placeholder="Password" {...password}/>
          {password.touched && password.error && <div className="validation-message">{password.error}</div>}
        </div>

        <div>
          <label>Confirme Password</label>
          <input type="password" placeholder="Confirme Password" {...confirmpass}/>
          {confirmpass.touched && confirmpass.error && <div className="validation-message">{confirmpass.error}</div>}
        </div>

        <button type="submit">Submit</button>
        { users.length ?
          (
            <ul>
              { users.map(({ username, _id }) => <li key={_id}>{username}</li>) }
            </ul>
          ) : null
        }
      </form>
    );
  }
}

User = reduxForm({ // <----- THIS IS THE IMPORTANT PART!
  form: 'newUser',                           // a unique name for this form
  fields: ['firstName', 'lastName', 'email', 'permission', 'username', 'phone', 'password', 'confirmpass'], // all the fields in your form
  validate,
})(User);

export default User;
