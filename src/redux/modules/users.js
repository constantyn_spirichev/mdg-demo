const LOAD_USERS = 'mdg/users/LOAD_USERS';
const LOAD_USERS_SUCCESS = 'mdg/users/LOAD_USERS_SUCCESS';
const LOAD_USERS_FAIL = 'mdg/users/LOAD_USERS_FAIL';
const SAVE_USER = 'mdg/users/SAVE_USER';
const SAVE_USER_SUCCESS = 'mdg/users/SAVE_USER_SUCCESS';
const SAVE_USER_FAIL = 'mdg/users/SAVE_USER_FAIL';

const initialState = {
  list: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case LOAD_USERS:
      return {
        ...state,
        loading: true,
      };
    case LOAD_USERS_SUCCESS: {
      return {
        ...state,
        loading: false,
        list: action.result,
      };
    }
    case LOAD_USERS_FAIL: {
      return {
        ...state,
        error: action.error,
        loading: false,
      };
    }
    default:
      return state;
  }
}


export function load() {
  return {
    types: [LOAD_USERS, LOAD_USERS_SUCCESS, LOAD_USERS_FAIL],
    promise: (client) => client.get('/users')
  };
}

export function save(newUser) {
  return {
    types: [SAVE_USER, SAVE_USER_SUCCESS, SAVE_USER_FAIL],
    promise: (client) => client.post('/users', {data: newUser}, 'form')
  };
}
