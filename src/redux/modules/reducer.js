import { combineReducers } from 'redux';
import { routerStateReducer } from 'redux-router';
import {reducer as formReducer} from 'redux-form';
import currentUser from './currentUser';
import surveys from './surveys';
import users from './users';

export default combineReducers({
  router: routerStateReducer,
  form: formReducer,
  currentUser,
  surveys,
  users,
});
