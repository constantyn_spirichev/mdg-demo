import { LOGIN_SUCCESS, LOGOUT } from 'redux/modules/currentUser';

export default () => next => action => {
  if (action.type === LOGIN_SUCCESS && __CLIENT__) {
    document.cookie = `token=${action.result.token}; path='/'`;
  }
  if (action.type === LOGOUT && __CLIENT__) {
    document.cookie = 'token=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=\'/\'';
  }
  return next(action);
};
