import React from 'react';
import {IndexRoute, Route} from 'react-router';
import {
    App,
    User
  } from 'containers';

export default (store) => {
  /**
   * Please keep routes in alphabetical order
   */
  return (
      <Route path="/" component={App} >
        <Route path="user" component={User} />
      </Route>
  );
};
